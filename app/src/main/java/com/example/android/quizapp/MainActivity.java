package com.example.android.quizapp;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    int screenHeight;
    int PageStep;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//This part will calculate the page heights
        Display display = getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
//      int screenWidth = size.x;
        screenHeight = size.y;
        PageStep = 0;
        LinearLayout Page1 = (LinearLayout) findViewById(R.id.Page1);
        LinearLayout.LayoutParams lp0 = (LinearLayout.LayoutParams) Page1.getLayoutParams();
        LinearLayout Quest1 = (LinearLayout) findViewById(R.id.quest1);
        LinearLayout.LayoutParams lp1 = (LinearLayout.LayoutParams) Quest1.getLayoutParams();
        LinearLayout Quest2 = (LinearLayout) findViewById(R.id.quest2);
        LinearLayout.LayoutParams lp2 = (LinearLayout.LayoutParams) Quest2.getLayoutParams();
        LinearLayout Quest3 = (LinearLayout) findViewById(R.id.quest3);
        LinearLayout.LayoutParams lp3 = (LinearLayout.LayoutParams) Quest3.getLayoutParams();
        LinearLayout Quest4 = (LinearLayout) findViewById(R.id.quest4);
        LinearLayout.LayoutParams lp4 = (LinearLayout.LayoutParams) Quest4.getLayoutParams();
        LinearLayout Quest5 = (LinearLayout) findViewById(R.id.quest5);
        LinearLayout.LayoutParams lp5 = (LinearLayout.LayoutParams) Quest5.getLayoutParams();

        FloatingActionButton next = (FloatingActionButton) findViewById(R.id.next);
        FloatingActionButton prev = (FloatingActionButton) findViewById(R.id.prev);
        next.setOnClickListener(new View.OnClickListener() {
            ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);

            @Override
            public void onClick(View view) {
//Here we define the page steppers
                Snackbar.make(view, R.string.nxQuest, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
                PageStep = PageStep + screenHeight;
                if (PageStep>(screenHeight*5)) {
                    PageStep = screenHeight*5;
                } else {
                    scrollView.smoothScrollTo(0, PageStep);
                }
            }
        });
        prev.setOnClickListener(new View.OnClickListener() {
            ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);

            @Override
            public void onClick(View view) {
//Here we define the page steppers
                Snackbar.make(view, R.string.prvQuest, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

                PageStep = PageStep - screenHeight;
                if (PageStep<0) {
                    PageStep = screenHeight;
                } else {
                    scrollView.smoothScrollTo(0, PageStep);
                }
            }
        });

//       int Page1Hight = ((int) Page1.getY()) + screenHeight;
//        TextView testX = (TextView) findViewById(R.id.testX);
//        ViewGroup.LayoutParams params = testX.getLayoutParams();
//        params.height = Page1Hight;
        int Page1Hight = screenHeight;


        lp0.height = Page1Hight;
        lp1.height = Page1Hight;
        lp2.height = Page1Hight;
        lp3.height = Page1Hight;
        lp4.height = Page1Hight;
        lp5.height = Page1Hight;

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        Spinner spinner = (Spinner) findViewById(R.id.spinner);
// Create an ArrayAdapter using the string array and a default spinner layout
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.planets_array, android.R.layout.simple_spinner_item);
// Specify the layout to use when the list of choices appears
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
// Apply the adapter to the spinner
        spinner.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_exit) {
            System.exit(0);
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView);
        if (id == R.id.nav_intro) {
            PageStep = 0;
            scrollView.smoothScrollTo(0, PageStep);
        } else if (id == R.id.nav_quest1) {
            PageStep = screenHeight;
            scrollView.smoothScrollTo(0, PageStep);
        } else if (id == R.id.nav_quest2) {
            PageStep = screenHeight*2;
            scrollView.smoothScrollTo(0, PageStep);
        } else if (id == R.id.nav_quest3) {
            PageStep = screenHeight*3;
            scrollView.smoothScrollTo(0, PageStep);
        } else if (id == R.id.nav_quest4) {
            PageStep = screenHeight * 4;
            scrollView.smoothScrollTo(0, PageStep);
        } else if (id == R.id.nav_quest5) {
                PageStep = screenHeight*5;
                scrollView.smoothScrollTo(0, PageStep);
            } else if (id == R.id.nav_share) {
            Intent sendIntent = new Intent();
            sendIntent.setAction(Intent.ACTION_SEND);
            sendIntent.putExtra(Intent.EXTRA_TEXT, "This is my text to send.");
            sendIntent.setType("text/plain");
            startActivity(sendIntent);
        } else if (id == R.id.nav_send) {
            Intent email = new Intent(Intent.ACTION_SEND);
            email.putExtra(Intent.EXTRA_EMAIL, new String[]{"youremail@yahoo.com"});
            email.putExtra(Intent.EXTRA_SUBJECT, "subject");
            email.putExtra(Intent.EXTRA_TEXT, "message");
            email.setType("message/rfc822");

            try {
                startActivity(Intent.createChooser(email, "Choose an Email client :"));
            } catch (ActivityNotFoundException e) {
                //TODO: Handle case where no email app is available
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    //Test all the answers.
    public void sendButton(View view){
        TextView sumText = (TextView) findViewById(R.id.sumText);
        RadioButton radioButton1 = (RadioButton) findViewById(R.id.radioButton1);
        Spinner spinner = (Spinner) findViewById(R.id.spinner);
        RadioButton radioButton5 = (RadioButton) findViewById(R.id.radioButton5);
        CheckBox checkbox2 = (CheckBox) findViewById(R.id.notify_me_checkbox2);
        CheckBox checkbox1 = (CheckBox) findViewById(R.id.notify_me_checkbox);
        Switch switchButton1 = (Switch) findViewById(R.id.switchButton1);
        Switch switchButton2 = (Switch) findViewById(R.id.switchButton2);
        TextView firstTextSum = (TextView) findViewById(R.id.firstTextSum);
        TextView secondTextSum = (TextView) findViewById(R.id.secondTextSum);
        TextView thirdTextSum = (TextView) findViewById(R.id.thirdTextSum);
        TextView fourthTextSum = (TextView) findViewById(R.id.fourthTextSum);
        TextView fifthTextSum = (TextView) findViewById(R.id.fifthTextSum);
        int countRight = 0;

        if (radioButton1.isChecked()) {
            firstTextSum.setText(R.string.YouGet);
            firstTextSum.setTextColor(Color.BLACK);
            countRight ++;
        } else {
            firstTextSum.setText(R.string.TryAgain);
            firstTextSum.setTextColor(Color.RED);
        }
        if (spinner.getSelectedItemId() == 1) {
            secondTextSum.setText(R.string.YouGet);
            secondTextSum.setTextColor(Color.BLACK);
            countRight ++;
        } else {
            secondTextSum.setText(R.string.TryAgain);
            secondTextSum.setTextColor(Color.RED);
        }
        if (radioButton5.isChecked()) {
            thirdTextSum.setText(R.string.YouGet);
            thirdTextSum.setTextColor(Color.BLACK);
            countRight ++;
        } else {
            thirdTextSum.setText(R.string.TryAgain);
            thirdTextSum.setTextColor(Color.RED);
        }
        if (checkbox2.isChecked()&!checkbox1.isChecked()) {
            fourthTextSum.setText(R.string.YouGet);
            fourthTextSum.setTextColor(Color.BLACK);
            countRight ++;
        } else {
            fourthTextSum.setText(R.string.TryAgain);
            fourthTextSum.setTextColor(Color.RED);
        }
        if (switchButton1.isChecked()&!switchButton2.isChecked()) {
            fifthTextSum.setText(R.string.YouGet);
            fifthTextSum.setTextColor(Color.BLACK);
            countRight ++;
        } else {
            fifthTextSum.setText(R.string.TryAgain);
            fifthTextSum.setTextColor(Color.RED);
        }

        sumText.setText(getString(R.string.Finish1) + countRight + getString(R.string.Finish2));

    }

}
